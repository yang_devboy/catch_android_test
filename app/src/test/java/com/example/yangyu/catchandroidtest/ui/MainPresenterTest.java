package com.example.yangyu.catchandroidtest.ui;

import com.example.yangyu.catchandroidtest.models.Record;
import com.example.yangyu.catchandroidtest.network.ApiService;

import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.mockito.runners.MockitoJUnitRunner;

import java.util.Collections;
import java.util.List;

import io.reactivex.Observable;

import static org.mockito.Mockito.*;
import static org.junit.Assert.*;

/**
 * A bunch of unit tests for the main presenter.
 */

@RunWith(MockitoJUnitRunner.class)
public class MainPresenterTest {
    @Rule
    public final RxSchedulersOverrideRule schedulers = new RxSchedulersOverrideRule();

    private MainPresenter mMainPresenter;

    @Mock
    private MainView mMainView;

    @Mock
    private ApiService mApiService;

    @Before
    public void setUp() throws Exception {
        MockitoAnnotations.initMocks(this);
        mMainPresenter = new MainPresenter(mApiService);
    }

    @Test
    public void test_loadJsonFile_success() throws Exception {
        mMainPresenter.attachView(mMainView);
        assertTrue(mMainPresenter.isViewAttached());

        when(mApiService.getJsonData()).thenReturn(Observable.just(Collections.<Record>emptyList()));
        mMainPresenter.loadJsonFile();

        verify(mMainView).showContent(anyListOf(Record.class));
        verify(mMainView).hideLoading();
    }

    @Test
    public void test_loadJsonFile_fail() throws Exception {
        mMainPresenter.attachView(mMainView);
        assertTrue(mMainPresenter.isViewAttached());

        when(mApiService.getJsonData())
                .thenReturn(Observable.<List<Record>>error(new RuntimeException()));
        mMainPresenter.loadJsonFile();

        verify(mMainView).hideLoading();
        verify(mMainView).showError(any(Throwable.class));
    }

    @Test
    public void test_detachView() throws Exception {
        mMainPresenter.attachView(mMainView);
        assertTrue(mMainPresenter.isViewAttached());

        mMainPresenter.detachView(true);
        assertFalse(mMainPresenter.isViewAttached());
    }
}