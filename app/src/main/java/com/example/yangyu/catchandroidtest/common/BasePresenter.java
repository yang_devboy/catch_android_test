package com.example.yangyu.catchandroidtest.common;

import com.hannesdorfmann.mosby3.mvp.MvpBasePresenter;

/**
 * Defines a common interface for all the presenters in the app.
 */

public class BasePresenter<V extends BaseView> extends MvpBasePresenter<V> {
}
