package com.example.yangyu.catchandroidtest.ui;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.Toast;

import com.example.yangyu.catchandroidtest.R;
import com.example.yangyu.catchandroidtest.common.BaseActivity;
import com.example.yangyu.catchandroidtest.common.BasePresenter;
import com.example.yangyu.catchandroidtest.di.components.DaggerMainActivityComponent;
import com.example.yangyu.catchandroidtest.di.components.MainActivityComponent;
import com.example.yangyu.catchandroidtest.di.modules.MainActivityModule;
import com.example.yangyu.catchandroidtest.models.Record;

import java.util.List;

import javax.inject.Inject;

import butterknife.BindView;

/**
 * The main activity.
 */

public final class MainActivity extends BaseActivity implements MainView,
        MainAdapter.OnClickListener,
        SwipeRefreshLayout.OnRefreshListener {
    @BindView(R.id.refresh_layout)
    SwipeRefreshLayout mRefreshLayout;
    @BindView(R.id.data_rv)
    RecyclerView mDataRecyclerView;
    @BindView(R.id.content_placeholder)
    FrameLayout mContentPlaceholder;

    @Inject
    MainPresenter mMainPresenter;

    @Inject
    MainAdapter mMainAdapter;

    @NonNull
    @Override
    public BasePresenter createPresenter() {
        return mMainPresenter;
    }

    @Override
    protected void injectDependencies() {
        final MainActivityComponent mainActivityComponent = DaggerMainActivityComponent.builder()
                .appComponent(getAppComponent())
                .mainActivityModule(new MainActivityModule(this))
                .build();
        mainActivityComponent.inject(this);
    }

    @Override
    protected int getLayoutResId() {
        return R.layout.activity_main;
    }

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mRefreshLayout.setOnRefreshListener(this);
    }

    @Override
    public void hideLoading() {
        if (mRefreshLayout.isRefreshing()) {
            mRefreshLayout.setRefreshing(false);
        }
    }

    @Override
    public void showContent(List<Record> records) {
        if (null == mDataRecyclerView.getAdapter()) {
            // the list has not been initialized
            mDataRecyclerView.setHasFixedSize(true);
            mDataRecyclerView.setLayoutManager(new LinearLayoutManager(this));
            mDataRecyclerView.setItemAnimator(new DefaultItemAnimator());
            mDataRecyclerView.setAdapter(mMainAdapter);
            mMainAdapter.setOnClickListener(this);
        }

        if (null != records) {
            mMainAdapter.replaceWith(records);
        }
    }

    @Override
    public void showError(Throwable throwable) {
        Toast.makeText(this, throwable.getMessage(), Toast.LENGTH_SHORT).show();
    }

    @Override
    public void onRefresh() {
        mMainPresenter.loadJsonFile();
    }

    @Override
    public void onItemClicked(Record record) {
        if (null != record) {
            mContentPlaceholder.setVisibility(View.VISIBLE);

            addFragmentToActivity(
                    DetailsFragment.newInstance(record.getContent()),
                    R.id.content_placeholder,
                    true,
                    android.R.anim.slide_in_left,
                    android.R.anim.slide_out_right
            );
        }
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();

        if (getSupportFragmentManager().getBackStackEntryCount() == 0) {
            mContentPlaceholder.setVisibility(View.GONE);
        }
    }
}
