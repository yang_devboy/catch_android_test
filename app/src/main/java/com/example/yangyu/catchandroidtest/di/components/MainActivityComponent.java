package com.example.yangyu.catchandroidtest.di.components;

import com.example.yangyu.catchandroidtest.di.modules.MainActivityModule;
import com.example.yangyu.catchandroidtest.di.scopes.PerActivity;
import com.example.yangyu.catchandroidtest.ui.MainActivity;
import com.example.yangyu.catchandroidtest.ui.MainAdapter;
import com.example.yangyu.catchandroidtest.ui.MainPresenter;

import dagger.Component;

/**
 * The component corresponding to the main activity.
 */

@PerActivity
@Component(dependencies = AppComponent.class, modules = MainActivityModule.class)
public interface MainActivityComponent {
    void inject(MainActivity activity);

    MainActivity mainActivity();

    MainPresenter mainPresenter();

    MainAdapter mainAdapter();
}
