package com.example.yangyu.catchandroidtest.network;

import com.example.yangyu.catchandroidtest.models.Record;

import java.util.List;

import io.reactivex.Observable;
import retrofit2.http.GET;

/**
 * This class defines the api endpoints used in the app.
 */

public interface ApiService {

    /**
     * Load the json file remotely.
     *
     * @return A list of records.
     */
    @GET("data/data.json")
    Observable<List<Record>> getJsonData();

}
