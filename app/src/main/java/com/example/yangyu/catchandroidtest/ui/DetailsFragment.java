package com.example.yangyu.catchandroidtest.ui;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.view.View;
import android.widget.TextView;

import com.example.yangyu.catchandroidtest.R;
import com.example.yangyu.catchandroidtest.common.BaseFragment;
import com.example.yangyu.catchandroidtest.common.BasePresenter;

import butterknife.BindView;

/**
 * The details fragment.
 */

public final class DetailsFragment extends BaseFragment {
    @BindView(R.id.details_tv)
    TextView mDetailsTextView;

    private static final String ARG_DETAILS = "ARG_DETAILS";

    private String mDetails;

    public static DetailsFragment newInstance(String details) {
        final DetailsFragment fragment = new DetailsFragment();
        final Bundle args = new Bundle();
        args.putString(ARG_DETAILS, details);
        fragment.setArguments(args);

        return fragment;
    }

    @NonNull
    @Override
    public BasePresenter createPresenter() {
        return new BasePresenter();
    }

    @Override
    protected void initArgs(Bundle args) {
        mDetails = args.getString(ARG_DETAILS);
    }

    @Override
    protected int getLayoutResId() {
        return R.layout.fragment_details;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        mDetailsTextView.setText(mDetails);
    }
}
