package com.example.yangyu.catchandroidtest.di.components;

import com.example.yangyu.catchandroidtest.di.modules.ApiModule;
import com.example.yangyu.catchandroidtest.di.modules.AppModule;
import com.example.yangyu.catchandroidtest.network.ApiService;

import javax.inject.Singleton;

import dagger.Component;

/**
 * The global application component that provides a single instance of the api network service.
 */

@Singleton
@Component(modules = {AppModule.class, ApiModule.class})
public interface AppComponent {

    ApiService apiService();

}
