package com.example.yangyu.catchandroidtest;

import android.app.Application;

import com.example.yangyu.catchandroidtest.di.components.AppComponent;
import com.example.yangyu.catchandroidtest.di.components.DaggerAppComponent;
import com.example.yangyu.catchandroidtest.di.modules.ApiModule;
import com.example.yangyu.catchandroidtest.di.modules.AppModule;
import com.orhanobut.logger.AndroidLogAdapter;
import com.orhanobut.logger.FormatStrategy;
import com.orhanobut.logger.Logger;
import com.orhanobut.logger.PrettyFormatStrategy;
import com.squareup.leakcanary.LeakCanary;
import com.squareup.leakcanary.RefWatcher;

/**
 * This is a subclass of Application used to provide shared objects for this app, and initialize
 * required third-party libraries.
 */

public class App extends Application {
    private AppComponent mAppComponent;

    @Override
    public void onCreate() {
        super.onCreate();
        initLeakCanary();
        initLogger();
        initAppComponent();
    }

    /**
     * Initialize the memory leak detection library.
     */
    protected RefWatcher initLeakCanary() {
        if (LeakCanary.isInAnalyzerProcess(this)) {
            // This process is dedicated to LeakCanary for heap analysis.
            // You should not init your app in this process.
            return RefWatcher.DISABLED;
        }
        return LeakCanary.install(this);
    }

    /**
     * Initialize the logger and this should be called only once. Logs will be hidden in a
     * production build.
     */
    private void initLogger() {
        final FormatStrategy formatStrategy = PrettyFormatStrategy.newBuilder()
                .tag(getString(R.string.app_name))
                .build();
        Logger.addLogAdapter(new AndroidLogAdapter(formatStrategy) {
            @Override
            public boolean isLoggable(int priority, String tag) {
                return BuildConfig.DEBUG;
            }
        });
    }

    /**
     * Initialize the application component.
     */
    private void initAppComponent() {
        mAppComponent = DaggerAppComponent.builder()
                .appModule(new AppModule(this))
                .apiModule(new ApiModule())
                .build();
    }

    public AppComponent getAppComponent() {
        return mAppComponent;
    }
}
