package com.example.yangyu.catchandroidtest.di.scopes;

import java.lang.annotation.Documented;
import java.lang.annotation.Retention;

import javax.inject.Scope;

import static java.lang.annotation.RetentionPolicy.RUNTIME;

/**
 * The scope that corresponds to the life cycle of an activity .
 */

@Scope
@Documented
@Retention(RUNTIME)
public @interface PerActivity {
}
