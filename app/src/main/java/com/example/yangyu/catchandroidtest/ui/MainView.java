package com.example.yangyu.catchandroidtest.ui;

import com.example.yangyu.catchandroidtest.common.BaseView;
import com.example.yangyu.catchandroidtest.models.Record;

import java.util.List;

/**
 * The view interface for the main activity.
 */

interface MainView extends BaseView {

    void hideLoading();

    void showContent(List<Record> records);

    void showError(Throwable throwable);

}
