package com.example.yangyu.catchandroidtest.ui;

import com.example.yangyu.catchandroidtest.common.BasePresenter;
import com.example.yangyu.catchandroidtest.di.scopes.PerActivity;
import com.example.yangyu.catchandroidtest.models.Record;
import com.example.yangyu.catchandroidtest.network.ApiService;
import com.orhanobut.logger.Logger;

import java.util.List;
import java.util.concurrent.TimeUnit;

import javax.inject.Inject;

import io.reactivex.Observer;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.annotations.NonNull;
import io.reactivex.disposables.Disposable;
import io.reactivex.schedulers.Schedulers;

/**
 * The presenter for the main activity.
 */

@PerActivity
public final class MainPresenter extends BasePresenter<MainView> {
    private final ApiService mApiService;

    private Disposable mDisposable;

    @Inject
    public MainPresenter(ApiService apiService) {
        mApiService = apiService;
    }

    /**
     * Load the remote json file.
     */
    void loadJsonFile() {
        if (isViewAttached()) {
            mApiService.getJsonData()
                    .throttleFirst(500, TimeUnit.MILLISECONDS)
                    .subscribeOn(Schedulers.io())
                    .unsubscribeOn(Schedulers.io())
                    .observeOn(AndroidSchedulers.mainThread())
                    .subscribe(new Observer<List<Record>>() {
                        @Override
                        public void onSubscribe(@NonNull Disposable d) {
                            mDisposable = d;
                        }

                        @Override
                        public void onNext(@NonNull List<Record> records) {
                            Logger.d("successfully loaded the remote json file");
                            if (isViewAttached()) {
                                getView().showContent(records);
                                getView().hideLoading();
                            }
                        }

                        @Override
                        public void onError(@NonNull Throwable e) {
                            Logger.e("Failed to load the remote json file");
                            if (isViewAttached()) {
                                getView().hideLoading();
                                getView().showError(e);
                            }
                        }

                        @Override
                        public void onComplete() {

                        }
                    });
        }
    }

    @Override
    public void detachView(boolean retainInstance) {
        super.detachView(retainInstance);
        if (null != mDisposable && !mDisposable.isDisposed()) {
            Logger.d("disposable will be disposed after detaching view");
            mDisposable.dispose();
        }
    }
}
