package com.example.yangyu.catchandroidtest.common;

import com.hannesdorfmann.mosby3.mvp.MvpView;

/**
 * Defines a common interface for all the views in the app.
 */

public interface BaseView extends MvpView {
}
