package com.example.yangyu.catchandroidtest.common;

/**
 * This class defines all the constants used in the app.
 */

public final class Constants {
    public static final String API_BASE_URL = "https://raw.githubusercontent.com/catchnz/android-test/master/";
}
