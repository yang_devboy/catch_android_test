package com.example.yangyu.catchandroidtest.di.qualifiers;

import java.lang.annotation.Documented;
import java.lang.annotation.Retention;

import javax.inject.Qualifier;

import static java.lang.annotation.RetentionPolicy.RUNTIME;

/**
 * The application related qualifier.
 */

@Qualifier
@Documented
@Retention(RUNTIME)
public @interface ForApplication {
}
