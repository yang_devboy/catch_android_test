package com.example.yangyu.catchandroidtest.di.modules;

import com.example.yangyu.catchandroidtest.BuildConfig;
import com.example.yangyu.catchandroidtest.common.Constants;
import com.example.yangyu.catchandroidtest.network.ApiService;

import java.util.concurrent.TimeUnit;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;
import okhttp3.OkHttpClient;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Retrofit;
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory;
import retrofit2.converter.gson.GsonConverterFactory;

/**
 * The module that provides a single instance of retrofit and api service.
 */

@Module
public final class ApiModule {
    /**
     * Retrofit instance settings.
     */
    private static final int CONNECT_TIMEOUT = 15;
    private static final int READ_TIMEOUT = 20;
    private static final int WRITE_TIMEOUT = 20;

    @Provides
    @Singleton
    String provideBaseUrl() {
        return Constants.API_BASE_URL;
    }

    @Provides
    @Singleton
    HttpLoggingInterceptor provideHttpLoggingInterceptor() {
        final HttpLoggingInterceptor logging = new HttpLoggingInterceptor();
        logging.setLevel(HttpLoggingInterceptor.Level.BODY);
        return logging;
    }

    @Provides
    @Singleton
    OkHttpClient provideOkHttpClient(HttpLoggingInterceptor logging) {
        final OkHttpClient.Builder httpClient = new OkHttpClient.Builder();
        // show logs for debug mode.
        if (BuildConfig.DEBUG) {
            httpClient.addInterceptor(logging);
        }
        // set different time out.
        httpClient.connectTimeout(CONNECT_TIMEOUT, TimeUnit.SECONDS);
        httpClient.readTimeout(READ_TIMEOUT, TimeUnit.SECONDS);
        httpClient.writeTimeout(WRITE_TIMEOUT, TimeUnit.SECONDS);
        httpClient.retryOnConnectionFailure(true);

        return httpClient.build();
    }

    @Provides
    @Singleton
    Retrofit provideRetrofit(String baseUrl, OkHttpClient client) {
        return new Retrofit.Builder()
                .baseUrl(baseUrl)
                .addConverterFactory(GsonConverterFactory.create())
                .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
                .client(client)
                .build();
    }

    @Provides
    @Singleton
    ApiService provideAPIService(Retrofit retrofit) {
        return retrofit.create(ApiService.class);
    }
}
