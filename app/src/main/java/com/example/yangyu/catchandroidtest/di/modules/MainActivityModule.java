package com.example.yangyu.catchandroidtest.di.modules;

import com.example.yangyu.catchandroidtest.di.scopes.PerActivity;
import com.example.yangyu.catchandroidtest.ui.MainActivity;

import dagger.Module;
import dagger.Provides;

/**
 * The module that corresponds to the main activity component.
 */

@Module
public final class MainActivityModule {
    private final MainActivity mActivity;

    public MainActivityModule(MainActivity activity) {
        mActivity = activity;
    }

    @Provides
    @PerActivity
    MainActivity provideMainActivity() {
        return mActivity;
    }
}
