package com.example.yangyu.catchandroidtest.common;

import android.os.Bundle;
import android.support.annotation.LayoutRes;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.hannesdorfmann.mosby3.mvp.MvpFragment;

import butterknife.ButterKnife;
import butterknife.Unbinder;

/**
 * Base class for all the fragments. All fragments in this app should subclass this class.
 */

public abstract class BaseFragment<V extends BaseView, P extends BasePresenter<V>> extends MvpFragment<V, P> {
    private Unbinder unbinder;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        final Bundle args = getArguments();
        if (null != args) {
            initArgs(args);
        }
    }

    /**
     * Initializes the fragment arguments if there are any. It is not necessary to override this
     * method when nothing is passed along.
     */
    protected void initArgs(Bundle args) {

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        return inflater.inflate(getLayoutResId(), container, false);
    }

    /**
     * Returns the layout resource identifier for this fragment.
     *
     * @return The layout resource identifier.
     */
    protected abstract @LayoutRes int getLayoutResId();

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        injectDependencies();
        super.onViewCreated(view, savedInstanceState);
        unbinder = ButterKnife.bind(this, view);
    }

    /**
     * Inject the dependencies.
     */
    protected void injectDependencies() {

    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        // when binding a fragment in onCreateView, set the views to null in onDestroyView.
        if (null != unbinder) {
            unbinder.unbind();
        }
    }
}
