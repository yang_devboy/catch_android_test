package com.example.yangyu.catchandroidtest.ui;

import android.support.annotation.Nullable;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.yangyu.catchandroidtest.R;
import com.example.yangyu.catchandroidtest.models.Record;

import java.util.Collections;
import java.util.List;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * The adapter for the list in main activity.
 */

public final class MainAdapter extends RecyclerView.Adapter<MainAdapter.ItemViewHolder> {
    private List<Record> mRecordList = Collections.emptyList();

    @Nullable
    private OnClickListener mOnClickListener;

    @Inject
    public MainAdapter() {

    }

    void setOnClickListener(@Nullable OnClickListener onClickListener) {
        mOnClickListener = onClickListener;
    }

    void replaceWith(List<Record> recordList) {
        mRecordList = recordList;
        notifyDataSetChanged();
    }

    @Override
    public int getItemCount() {
        return mRecordList.size();
    }

    @Override
    public MainAdapter.ItemViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        return new ItemViewHolder(LayoutInflater.from(parent.getContext())
                .inflate(R.layout.layout_list_item_json_data, parent, false));
    }

    @Override
    public void onBindViewHolder(MainAdapter.ItemViewHolder holder, int position) {
        final Record record = mRecordList.get(position);
        if (null != record) {
            holder.mTitleTextView.setText(record.getTitle());
            holder.mSubtitleTextView.setText(record.getSubtitle());
            holder.mArrowImageView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    if (mOnClickListener != null) {
                        mOnClickListener.onItemClicked(record);
                    }
                }
            });
        }
    }

    /**
     * Provide a reference to the view for the list's item.
     */
    static final class ItemViewHolder extends RecyclerView.ViewHolder {
        @BindView(R.id.title_tv)
        TextView mTitleTextView;
        @BindView(R.id.subtitle_tv)
        TextView mSubtitleTextView;
        @BindView(R.id.arrow_iv)
        ImageView mArrowImageView;

        ItemViewHolder(View v) {
            super(v);
            ButterKnife.bind(this, v);
        }
    }

    interface OnClickListener {
        void onItemClicked(Record record);
    }
}
