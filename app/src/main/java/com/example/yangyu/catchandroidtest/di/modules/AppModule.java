package com.example.yangyu.catchandroidtest.di.modules;

import android.app.Application;

import com.example.yangyu.catchandroidtest.App;
import com.example.yangyu.catchandroidtest.di.qualifiers.ForApplication;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;

/**
 * The module that corresponds to the application component.
 */

@Module
public final class AppModule {
    private final App mApp;

    public AppModule(final App app) {
        mApp = app;
    }

    @Provides
    @Singleton
    @ForApplication
    Application provideApplication() {
        return mApp;
    }
}
